package Manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import DataStructures.DoubleLinkedList;
import DataStructures.LinearProbingHashST;
import DataStructures.RedBlackTree;
import DataStructures.SeparateChainingHashST;
import VO.VOStop;
import VO.VOStopTime;
import VO.VOTrip;

public class Manager {
	  private File archivo;
	  SeparateChainingHashST<Integer, VOTrip>Trips=new SeparateChainingHashST<Integer,VOTrip>(6007);
	  LinearProbingHashST<Integer, VOStop> stopsLp= new LinearProbingHashST<Integer, VOStop>(3001);
	  		

	  public void loadTrips(String tripsFile) throws IOException
	  {	
	  	archivo = new File (tripsFile);
	  	if (archivo==null)
	  	{
	  		throw new IOException("El archivo que se intenta abrir es nulo");
	  	}
	  	else
	  	{
	  		
	  	FileReader reader = new FileReader (archivo);
	  	
	  	BufferedReader lector = new BufferedReader(reader);
	  	String Linea=lector.readLine();
	  	Linea=lector.readLine();
	  	
	  	while (Linea!=null)
	  	{	
	  		String[] a= Linea.split(",");

	  		int route=Integer.parseInt(a[0]);
	  		int ser=Integer.parseInt(a[1]);
	  		int trip=Integer.parseInt(a[2]);
	  	    String Name=a[3];
	  		int shape=Integer.parseInt(a[7]);
	  		
	  		VOTrip t= new VOTrip(route, ser, shape, trip, Name);
	  		Trips.put(trip, t);		
	  		Linea=lector.readLine();
	  	}
	  	lector.close();
	  	reader.close();
	  	
	  }
	  	System.out.println("Trips: "+Trips.size());}

	  public void loadStopsLp(String stopsFile) throws IOException
	  {
	  	archivo= new File(stopsFile);
	  	if (archivo==null)
	  	{			throw new IOException("El archivo que se intenta abrir es nulo");	}
	  	else
	  	{
	  	FileReader reader = new FileReader (archivo);
	  	
	  	BufferedReader lector = new BufferedReader(reader);
	  	String Linea=lector.readLine();
	  	Linea=lector.readLine();
	  	while (Linea!=null)
	  	{
	  		String[] a= Linea.split(",");
	  		
	  		Integer Id =0;
	  		int code = 0;
	  		if(a[0].equals(" "))
	  		{
	  			System.out.println(a[2] +" no tiene id");
	  		}
	  		else if(a[0].equals(" ")==false)
	  		{
	  			Id=(Integer) Integer.parseInt(a[0]);
	  		}
	  		else if(a[1].equals(" "))
	  		{
	  			System.out.println(a[2] +" no tiene c�digo");
	  		}
	  		else
	  		{
	  			code=Integer.parseInt(a[1]);
	  		}
	  			
	  	    String name=a[2];
	  		double lat=Double.parseDouble(a[4]);
	  		double lon=Double.parseDouble(a[5]);
	  	    String zoneId=a[6];
	  	    DoubleLinkedList<VOStopTime> stopTimes = new DoubleLinkedList<VOStopTime>();
	  	    
	  	    VOStop stop= new VOStop(Id, code, name, lat, lon, zoneId, stopTimes);
	  	    stopsLp.put(Id, stop);
	  	    Linea=lector.readLine();
	  	}
	  	lector.close();
	  	} System.out.println("Stops: "+stopsLp.size());
	  }

	  public void loadStopTimes(String stopTimesFile) throws IOException
	  {
	  	archivo = new File (stopTimesFile);
	  	if (archivo==null)
	  	{			throw new IOException("El archivo que se intenta abrir es nulo");
	  	}
	  	else
	  	{
	  	FileReader reader = new FileReader (archivo);
	  	
	  	BufferedReader lector = new BufferedReader(reader);
	  	String Linea=lector.readLine();
	  	Linea=lector.readLine();
	  	while (Linea!=null)
	  	{
	  		String[] a= Linea.split(",");
	  		int tripId=Integer.parseInt(a[0]);
//	  		if(a[1].charAt(0)==' ')
//	  	    {
	  			String arrTime=a[1].replace(" ", "");
//	  	    }
	  	    String depTime=a[2].replace(" ", "");
	  		Integer stopId=(Integer) Integer.parseInt(a[3]);
	  		int stopSequence=Integer.parseInt(a[4]);
	  		double distTraveled=0;
	  		if(a.length==7)
	  		{
	  			
	  			distTraveled=Double.parseDouble(a[8]);
	  		}

	  	    VOStopTime sTime= new VOStopTime(tripId, arrTime, depTime, stopId, stopSequence, distTraveled);
	  	    VOStop stop = stopsLp.get(stopId);
	  	    DoubleLinkedList<VOStopTime> st= stop.stopTimes();
	  	    st.add(sTime);
	  	    Linea=lector.readLine();
	  	}
	  	lector.close();
	  	}
	  }

	  public RedBlackTree<String, VOTrip> viajesXparadasOrg(int stopId)
	  {
	  	RedBlackTree<String, VOTrip> arbol = new RedBlackTree<>();
	  	DoubleLinkedList<VOStopTime> viajes = stopsLp.get(stopId).stopTimes();
	  	
	  	Iterator<VOStopTime> iter = viajes.iterator();
	  	while(iter.hasNext())
	  	{
	  		VOStopTime st = iter.next();
	  		VOTrip viaje = Trips.get(st.TripId());
	  		arbol.Put(st.ArrivalTime(), viaje);
	  	}
	  
	  	return arbol;
	  }

	  public LinearProbingHashST<Integer, DoubleLinkedList<VOTrip>> paradasRangoHora(String horaIn, String  horaFin, int stopId)
	  {
	  	RedBlackTree<String, VOTrip> trips = viajesXparadasOrg(stopId);

	  	LinearProbingHashST<Integer, DoubleLinkedList<VOTrip>> res = new LinearProbingHashST<>(51);
	  	System.out.println("hora in: "+horaIn+ ", hora fin: "+ horaFin);
	  	Iterable<VOTrip> viajes = trips.valuesInRange(horaIn, horaFin);
	  	Iterator<VOTrip> iter= viajes.iterator();

	  	while(iter.hasNext())
	  	{
	  		VOTrip viaje = iter.next();
	  		int ruta= viaje.RouteId();
	  		if(!res.contains(ruta))
	  		{
	  			res.put(ruta, null);
	  		}
	  		DoubleLinkedList<VOTrip> trList= res.get(ruta);
	  		if(trList==null)
	  		{
	  			trList= new DoubleLinkedList<>();
	  			trList.add(viaje);
	  			res.put(ruta, trList);
	  		}
	  		else
	  		{
	  			trList.add(viaje);
	  		}
	  	}
	  	return res;
	  	
	  }
}
