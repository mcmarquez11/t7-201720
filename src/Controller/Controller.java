package Controller;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import DataStructures.DoubleLinkedList;
import DataStructures.LinearProbingHashST;
import DataStructures.RedBlackTree;
import Manager.Manager;
import VO.VOTrip;


public class Controller {
	/**
	 * Reference to the routes and stops manager
	 */
	private static Manager  manager = new Manager();

	public static void loadStops(String stopsFile) throws IOException 
	{
		manager.loadStopsLp(stopsFile);
	}

	public static void loadStopTimes(String stopTimesFile) throws IOException	
	{
		manager.loadStopTimes(stopTimesFile);
	}
	
	public static void loadTrips(String tripsFile) throws IOException 
	{
		manager.loadTrips(tripsFile);
	}
	
	public static void viajesXparada(Integer stopId)
	{
		RedBlackTree<String, VOTrip> arbol =manager.viajesXparadasOrg(stopId);
	  	
		DoubleLinkedList<VOTrip> ordenado= arbol.inOrder();
	  	Iterator<VOTrip> iterator=ordenado.iterator();
	  	System.out.println("Los viajes que paran en la parada "+stopId+ " organizados por hora de llegada son:");
	  	while (iterator.hasNext())
	  	{
	  		VOTrip act = iterator.next();
	  		System.out.println("TripId: "+act.TripId()+" RouteId: "+ act.RouteId());
	  	}
	}
	
	public static void paradasRangoHora(String horaIn, String horaFin, int stopId)
	{
		LinearProbingHashST<Integer, DoubleLinkedList<VOTrip>> res = manager.paradasRangoHora(horaIn, horaFin, stopId);
		Iterable<Integer> keys = res.keys();
		Iterator<Integer> iter = keys.iterator();
		System.out.println("Los viajes que paran en la estaci�n "+stopId+ " entre las horas "+horaIn+ " y "+horaFin+ " son: ");
		while(iter.hasNext())
		{
			int ruta =iter.next();
			System.out.println("Ruta "+ ruta+ ": " );
			DoubleLinkedList<VOTrip> viajes = res.get(ruta);
			Iterator<VOTrip> trIter = viajes.iterator();
			while(trIter.hasNext())
			{
				VOTrip viaje = trIter.next();
				System.out.println(viaje.TripId());
			}
		}
	}

}
