package DataStructures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();
	
	public void add(T t);
	
	public void addAtEnd(T t);
	
	public void AddAtk(int k,T t) throws Exception;

	public T getElement(int k);
	
	void deleteATk(int k);
	public void setElement(int k, T aa);
	

}
