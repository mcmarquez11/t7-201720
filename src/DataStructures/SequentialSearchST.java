package DataStructures;

import DataStructures.DoubleLinkedList.Node;

public class SequentialSearchST<Key, Value> {
	
	private Node first;
	int Size;
	private class Node
	{
		Key key;
		Value val;
		Node next;
		
		public Node(Key key, Value val, Node next)
		{
			this.key=key;
			this.val=val;
			this.next=next;
		}
	}
	
	public Value get(Key key)
	{
		Node a=first;
		while(a!=null)
		{
			
			if(key.equals(a.key))
			{
				return a.val;	}
			a=a.next;
		}
		return null;
	}
	
	public boolean put(Key key, Value val)
	{
		boolean j=true;
		Node a = first;
		while(a!=null)
		{
			if(key.equals(a.key))
			{
				a.val=val;
				j=false;
				return j;
			}
			a=a.next;
		}
		first=new Node(key, val, first);
		Size++;
		return j;
	}
	
	public int Size()
	{return Size;}
	
	private Node getNode(int k)throws Exception {
		if(k >=Size)
		{
			throw new Exception("Intenta acceder a un elemento inexistente");
			
		}
		else{
		Node a =first;
		
	for (int i=0 ; i<k;i++)
	{
		a=a.next;
			
		}
		
		return a;}
	}
	public Value getElement(int k) {
	try{
		Node a= getNode(k);
		Value b= a.val;
		return b;
	}
	catch (Exception e){
		return null;
	}
	}
	public void deleteATk(int k) {
		try{
		 Node  a=getNode(k);
            	
		 Node c= a.next;
		 if(k>0)
		 {
			 Node b=getNode(k-1);
			 b.next=c;
		 }
		 else
		 {
			 first=c;
		 }
		 
		Size--;}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}



}
