package DataStructures.Test;

import static org.junit.Assert.*;

import junit.framework.TestCase;
import DataStructures.RedBlackTree;
import DataStructures.DoubleLinkedList;

import junit.framework.TestCase;

public class RedBlackTreeTest extends TestCase{

	 RedBlackTree<Integer,Integer> tree =new RedBlackTree<Integer,Integer>();
	  Integer[]a=new Integer[10];
public void SetupEscenario1()
{
	
		a[1]=61;
		a[0]=51;
		a[2]=123;
		a[3]=9621;
		a[4]=652;
		a[5]=20;
		a[6]=21;
		a[7]=143;
		a[8]=40;
		a[9]=416;
		
		
		for (int i=0;i<10;i++)
		{
			tree.Put(a[i], a[i]*7);
	
		}
	
}
	
	public void testPut() {
		SetupEscenario1();
		assertNotNull(tree);
		assertEquals(10, tree.size());}
		
	public void testmax()
	{SetupEscenario1();
	assertEquals(9621,(int) tree.max());
	}
	public void testmin()
	{SetupEscenario1();
	assertEquals(20,(int) tree.min());
	}
	public void testDeleteMax()
	{SetupEscenario1();
	tree.DeleteMax();
	tree.DeleteMax();
	assertEquals(416,(int)tree.max() );
	}
	public void testDeleteMin()
	{SetupEscenario1();
	tree.DeleteMin();
	tree.DeleteMin();
	assertEquals(40,(int)tree.min() );
	}
	
	
	public void testGet()
	{SetupEscenario1();

	assertEquals(51*7, (int)tree.get(51));
	assertEquals(9621*7, (int)tree.get(9621));
		
	}
	
	
	public void testRangeK() {
		SetupEscenario1();
         DoubleLinkedList<Integer> A= (DoubleLinkedList<Integer>)tree.keysInRange(1000, 10000);
		 for(int i=0;i<A.getSize();i++)
		 {
				assertEquals(1 ,(int)A.getSize());
				assertEquals(9621 ,(int)A.getElement(0));
		 }
	}
	
	public void testRangeV() {
		SetupEscenario1();
         DoubleLinkedList<Integer> A= (DoubleLinkedList<Integer>)tree.valuesInRange(10,100);
		 for(int i=0;i<A.getSize();i++)
		 {   
			 assertEquals(5 ,(int)A.getSize());
			// System.out.println(A.getElement(i));
		 }
	}
	public void testDelete (){
		SetupEscenario1();
		tree.delete(143);
		assertEquals(9 ,(int)tree.size());
		
		try {tree.delete(143);}
		catch(Exception e)
		{
			System.out.println("$$$$" +"ERROR" +"no se encuentra el elemento  eliminar");
		}
		assertEquals(9 ,(int)tree.size());
		tree.delete(20);
		assertEquals(21 ,(int)tree.min());

		 }
	public void testHeight(){
         SetupEscenario1();
		assertEquals(2 ,tree.height());
		RedBlackTree<Integer,Integer>t2=new RedBlackTree<Integer,Integer>();
		t2.Put(64, 0);
		assertEquals(0,t2.height());
		t2.Put(32, 0);
		assertEquals(0,t2.height());
		t2.Put(96, 0);
		assertEquals(1,t2.height());
		
		 }
	public void  testkeys()
			
	{ SetupEscenario1();
	DoubleLinkedList<Integer> II=(DoubleLinkedList<Integer>)tree.keys();

		
		for(int i=0;i<a.length;i++)
		{
			assertEquals(a[i],II.getElement(i));
			
			
		}
	}
	public void testcontains()
	{SetupEscenario1();
		assertTrue(tree.Contains(21));
		assertTrue(tree.Contains(416));
		
		
		assertFalse(tree.Contains(999));
		assertFalse(tree.Contains(7999));
		
	}
	

	

}
