package DataStructures.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import DataStructures.DoubleLinkedList;
import DataStructures.SeparateChainingHashST;
import junit.framework.TestCase;

public class SeparateChainingHashTest<K,V> extends TestCase{

        K k1;
        V V1;
	 
		SeparateChainingHashST<K,V>a;
		SeparateChainingHashST<Integer,String>b;
		DoubleLinkedList<K>K1=new DoubleLinkedList<K>();
		
		public void setupEscenario1( )
	    {    
		a=new SeparateChainingHashST<K,V>(101);
		b=new SeparateChainingHashST<Integer,String>(101);	

	    }
		
		public void testSeparateChainigHsg() {
			  setupEscenario1();
			  assertNotNull( "La  ST a  no deberia ser nula", a );
			  assertNotNull( "La  ST b  no deberia ser nula",b );
		}
		//prueba los m�todos put y get
		public void testPutGet () {
			  setupEscenario1();
			 int[]i1=new int[101];
			 String[]S1=new String[101];
		for (int i=0;i<1000;i++)
			
		{ double k= Math.random()*100;
		 Integer l=(int)k;
		  int kk=(l.hashCode() & 0x7fffffff)%101;  
		  i1[kk]=(int)k;
		  char c1=(char)(64+i%63); 
		  char c2=(char)(64+(i*i1[kk])%63); 
		  char c3=(char)(64+i%15); 
		  char c4=(char)(64+i%25); 
		  String ss=""+c1+c2+c3+c4;
		  
		 
		  S1[kk]=ss;
		 b.put((int)k, ss);
		}
		
		 									
			for (int i=0;i<102;i++)
			{
			Integer l=i;	
		    int kk=(l.hashCode() & 0x7fffffff)%101; 
		    
		     String  Y=b.get(kk);
             if(Y!=null)
             {assertTrue(i+" "+Y+"espacio "+ S1[kk]+"" ,Y.equals(S1[kk]) );}
		    
             }
			
		}
		
		
		public void testRehash(){
			  setupEscenario1();
			double a=b.size()/b.M();
			assertTrue(a<6);
			
			
			for (int i=0;i<200;i++)
			{ int[]i1=new int[b.M()];
			 String[]S1=new String[b.M()];
			 double k= Math.random()*100;
			 Integer l=(int)k;
			  int kk=(l.hashCode() & 0x7fffffff)%101;  
			  i1[kk]=(int)k;
			  char c1=(char)(64+i%63); 
			  char c2=(char)(64+(i*i1[kk])%63); 
			  char c3=(char)(64+i%15); 
			  char c4=(char)(64+i%25); 
			  String ss=""+c1+c2+c3+c4;
			   S1[kk]=ss;
			 b.put((int)k, ss);
			}
			 a=b.size()/b.M();
			assertTrue(a<6);
		}
		
		public void testdelete()
		{  setupEscenario1();
			b.put(99, "abc");
		 {assertTrue("Deberia se abc" ,b.get(99).equals("abc"));}
		 
		 b.Delete(99);
		 System.out.println(b.get(99));
		 assertNull("deberia ser un objeto Nulo", b.get(99));
			
			
		}
		public void testkeys()
		{
			setupEscenario1();
			for(int i=0;i<200;i++)
		       {       
				  char c1=(char)(64+i%63); 
				  char c3=(char)(64+i%15); 
				  char c4=(char)(64+i%25); 
				  String ss=""+c1+c3+c4;
				  b.put(i, ss);
		       }
			DoubleLinkedList<Integer>ll=(DoubleLinkedList)b.keys();
			for(int i=0;i<200;i++)
		       {       
				assertTrue(i==ll.getElement(i));
		       }
			
		}
			
		

	}


