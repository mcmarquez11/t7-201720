package DataStructures.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import DataStructures.Heap;
import DataStructures.LinearProbingHashST;
import junit.framework.TestCase;
//prueba los metodos de las dos clases de heap. HeapMax y HeapMin
public class HeapTest <k extends Comparable<k>>extends  TestCase{
	private Heap <Integer> Max;
	private Heap <Integer> Min;
	private int []keys;
	public void SetupEscenario1()
	{   keys=new int[10];
		keys[0]=999;
		keys[1]=980;
		keys[2]=176;
		keys[3]=83;
		keys[4]=24;
		keys[5]=7;
       	keys[6]=2246;
       	keys[7]=50;
		keys[8]=83;
		keys[9]=100;
		Max=new Heap<Integer>(20, true);
		Min=new Heap<Integer>(20, false);
		  
		
		for (int i=0;i<10;i++)
		    {Max.insert(keys[i]);
		     Min.insert(keys[i]); }
      
		
	}
	public void testHeap()
	{ 

		SetupEscenario1();
	
	    assertNotNull(Max);
	    assertNotNull(Min);

	}


	
	public void testinsert()
	{ 
		SetupEscenario1();
		
		Max.insert(1220);
		//System.out.println(Max.minmax()+"Q");
		Min.insert(2);
		  
	  
	    assertEquals(11, Max.size());
	    assertEquals(11, Min.size());

	}



public void testMaxMin()
	{ 
	SetupEscenario1();
		   
	int a=Max.minmax();
	System.out.println(a);
	 assertEquals(2246, a);
	 Max.insert(2);
	 a=Max.minmax();
	 assertEquals(999, a);
	}

			
	

}
