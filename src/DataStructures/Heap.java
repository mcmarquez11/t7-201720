package DataStructures;
// Crea una  Heap orientado a mayor o menor seg�n parametro con tama�o seg�n parametro.
//Heap implementado en arreglos; 
public class Heap<k extends Comparable<k>> {
	
	private boolean max;
	private k[] keys;
	private int size =0;
	public boolean empty ()
	{
		if(size==0)
			return true;
		else
			return false;
	}

	public int size()
	{return size;}
	public Heap(int s,boolean m)
	{
		keys=(k[]) new Comparable [s+1];
		max=m;
	}
	
	public void swim(int i){
		if(max==true){
		
	
			while(i>1&&(keys[i].compareTo(keys[i/2])>0) )
			{exchange(i, i/2);
			i=i/2;}}
		
		
		else{	
			while(i>1&&(keys[i].compareTo(keys[i/2])<0) )
			{exchange(i, i/2);}}
		       i=i/2;
				}
		
		
	
	public void sink(int i)
	{
		if(max==true){
		
		while(2*i<=size)
		{int j=2*i;
		
		if(j<size&&(keys[j].compareTo(keys[j+1])<0))
		{
		j=j+1;
		}
		if(keys[i].compareTo(keys[j])<0)
		{ exchange(i, j); 
		i=j;}
		else {break;}
		}}
		
		else
		{
		while(2*i<=size)
		{int j=2*i;
		
		if(j<size&&(keys[j].compareTo(keys[j+1])>0))
		{
		j=j+1;
		}
		if(keys[i].compareTo(keys[j])>0)
             { exchange(i, j); 
             i=j;}
		else{break;}
		}
			
		}
			
				
	}
	
	public void exchange(int a , int b)
	
	{k c=keys[a];
	keys[a]=keys[b];
	keys[b]=c;
		
	}
	public k minmax()
	{
		k j=keys[1];
		exchange(1,size);
		size--;
		keys[size+1]=null;
		sink(1);
		return j;
	
	}
	
	public void insert(k K)
	{     size++;
		keys[size]=K;
		swim(size);
		
	}
	public boolean max()
	{
		return max;
	}
}
