package DataStructures;

import java.util.NoSuchElementException;

import DataStructures.DoubleLinkedList;

public class RedBlackTree<K extends Comparable<K>,V >  {
    DoubleLinkedList<K> keys=new DoubleLinkedList<K>();
	private node root;
	
	public class node 
	{ K key;
	  V value;
	  node right;
	  node left;
	  boolean red;
	  int size;
	  public node(K k,V v ,int s, boolean color)
	  {	  this.key=k;
		  this.value=v;
		  this.size=s;
		  this.red=color;
		  		  
	  }
	}
	  public int size()
	  {
		  return size(root);
		  
	  }
	  private int size(node a)
	  {
		  if(a==null) return 0;
		  else return a.size;
	  }
	
	
	
	private void flip(node a)
	{   a.red=true;
		a.left.red=false;
		a.right.red=false;
			
	}
     private node Rright(node a)
	{   node g =a.left;
	    a.left=g.right;
	    g.right=a;
	    g.red=a.red;
	    
	    a.red=true;
	    g.size=a.size;
	    a.size=1+size(a.left)+size(a.right);
		return g;	
	}
	private node Rleft(node a)
	{  node b=a.right;
	   a.right=b.left;
	   b.left=a;
	   b.red=a.red;
	   a.red=true;
	   b.size=a.size;
	   a.size=1+size(a.left)+size(a.right);
	   return b;
	}
			
	

	public boolean isRed(node a)
	{ if(a==null)
	{return false;}
		return a.red==true;
	}
	public void Put( K k,V v)
	{
		root=put(root,k,v);
		root.red=false;
	}
	
	private node put(node a,K k,V v)
	{
		if(a==null)
			{keys.addAtEnd(k);
		
		//	System.out.println("agrega "+ k);
			return new node(k,v,1,true);}
		int comp=k.compareTo(a.key);
		if(comp<0){a.left=put(a.left,k,v);}
		else if(comp>0){a.right=put(a.right,k,v);}
		else a.value=v;
		
		if(isRed(a.right)&&!isRed(a.left))
		{ 
			a=Rleft(a);
		}
		if(isRed(a.left)&&isRed(a.left.left))
		{ 
			a=Rright(a);}
		if(isRed(a.left)&&isRed(a.right))
		
		{
		flip(a);}
	
		a.size=size(a.left)+size(a.right)+1;
		return a;
	}
	public K min()
	{
		return Min(root).key;
		
	}
	private node Max(node a)
	{ 
		if( a.right ==null)
		{return a;}
		return Max(a.right);
	}
	public K max()
	{
		return Max(root).key;		
	}
	private node Min(node a)
	{ 
		if( a.left ==null)
		{return a;}
		return Min(a.left);
	}
	
	public K floor(K k)

	{
		node a=Floor(root,k);
		if(a==null) 
		{return null;}
		return a.key;
	}
	
	private node Floor(node a , K k)
	{
		if(a==null)
			{return null;}
		int comp=k.compareTo(a.key);
		if(comp==0)
		{return a;}
		if(comp<0)
		{ return Floor(a.left,k);}
		node b=Floor(a.right,k);
		if(b!=null) 
		{return b;}
		else return a;
		
		
	}
	public K ceiling(K k)

	{
		node a=Ceiling(root,k);
		if(a==null)
			{return null;}
		return a.key;
	}
	private node Ceiling(node a , K k)
	{
		if(a==null)return null;
		int comp=k.compareTo(a.key);
		if(comp==0)
		{return a;}
		if(comp>0) return Ceiling(a.right,k);
		node b=Ceiling(a.left,k);
		if(b!=null) 
			{return b;}
		else return a;
		
		
	}
	private node Deletemin(node a)
	{
	 if (a.left == null) 
		 {return a.right;}
	 a.left = Deletemin(a.left);
	 a.size = size(a.left) + size(a.right) + 1;
	 return a;
	}
	public void DeleteMin()
	{
		root=Deletemin(root);
	}
	public void DeleteMax()
	{
		root=Deletemax(root);
	}
	private node Deletemax(node a)
	{
	 if (a.right == null) 
		 {return a.left;}
	 a.right = Deletemax(a.right);
	 a.size = size(a.left) + size(a.right) + 1;
	 return a;
	}
	public V get(K key)
	{ return Get(root, key); }
	private V Get(node a, K key)
	{
	 if (a == null) 
		 {return null;}
	 int comp = key.compareTo(a.key);
	 if (comp < 0) 
		 {return Get(a.left, key);}
	 else if (comp > 0) 
		 {return Get(a.right, key);}
	 else 
		 {return a.value;}
	}
	public Iterable<K>keysInRange(K a,K b) 
	{DoubleLinkedList<K>k=new DoubleLinkedList<K>();
	RangeK(root, k, a, b);
	return k;
	}

private void RangeK(node A, DoubleLinkedList<K>List, K a, K b)
{
 if (A == null)
	 {return;}
 int low = a.compareTo(A.key);
 int high = b.compareTo(A.key);
 if (low < 0)
{RangeK(A.left, List, a, b);}
 if (low <= 0 && high >= 0) 
	 {List.addAtEnd(A.key);}
 if (high > 0)
{RangeK(A.right, List, a, b);}
}
public DoubleLinkedList<V>valuesInRange(K a,K b) 
{DoubleLinkedList<V>k=new DoubleLinkedList<V>();
RangeV(root, k, a, b);
return k;
}

public void delete(K key)
{ root = Delete(root, key); }
private void RangeV(node A, DoubleLinkedList<V>List, K a, K b)
{
if (A == null) 
	{return;}
int low = a.compareTo( A.key);
int high = b.compareTo(A.key);
if (low < 0)
	{RangeV(A.left, List, a, b);}
if (low <= 0 && high >= 0)
	{List.addAtEnd(A.value);}
if (high > 0) RangeV(A.right, List, a, b);
}
private node Delete(node a, K key) throws NoSuchElementException
{
 if (a == null) 
	 {throw new NoSuchElementException();
	// return null;}
	 }
 int cmp = key.compareTo(a.key);
 if (cmp < 0)
	 {a.left = Delete(a.left, key);}
 else if (cmp > 0) a.right = Delete(a.right, key);
 else
 {
 if (a.right == null) 
	 {return a.left;}
 if (a.left == null) 
	 {return a.right;}
  node t = a;
 a = Min(t.right); 
 a.right = Deletemin(t.right);
 a.left = t.left;
 }
 a.size = size(a.left) + size(a.right) + 1;
 return a;
}
public int height()
{int h=0;
h=Height(root,h);
return h-1;
	
	}

private int Height(node N,int h)
{ if(N==null)
{return 0;	}


 h=h+Height(N.right,h);
 if(N.red==false)
 {h++;}
	return h;
}
public Iterable<K> keys()
{return keys;
	}

public boolean Contains(K k)
{  boolean a =false;
  a=contains(root,k,a); 
	return a;
}
public boolean contains(node a,K key,boolean u)
{
	
		 if (a == null) 
			 {return false;}
		 int comp = key.compareTo(a.key);
		 if (comp < 0) 
			 {return contains(a.left, key,u);}
		 else if (comp > 0) 
			 {return contains(a.right, key,u);}
		 else 
			 {return true;}
		}	

	
	public DoubleLinkedList<V> inOrder()
	{
		DoubleLinkedList<V> res = new DoubleLinkedList<V>();
		InOrder(res, root);
		return res;
	}
	
	private void InOrder(DoubleLinkedList<V> list, node n  )
	{
		if (n==null)return;
		InOrder(list,n.left);
		list.addAtEnd(n.value);
		InOrder(list, n.right);
	}
	
}
