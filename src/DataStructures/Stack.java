package DataStructures;

public class Stack<E> implements IStack<E> {
	private int size=0;
	private Node  First;
	
	
	private class Node{
	E Object;
	Node next;
	private Node (E object)
	{		 Object=object;}
	}
			
	

	@Override
	public void push(E item) {
	Node a= new Node(item);
	a.next=First;
	First=a;
	size ++;	

		
	}

	@Override
	public E pop() {

	E obj=First.Object;
	First=First.next;	
		
		size --;
		return obj;
	}
	public int Size()
	{return size;}
		
	
}
