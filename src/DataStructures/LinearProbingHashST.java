package DataStructures;

public class LinearProbingHashST<Key, Value> {

	private int M;
	private int N;
	private Key[] keys;
	private Value[] values;
	
	public LinearProbingHashST()
	{
		this(995);
	}
	public LinearProbingHashST(int M)
	{
		this.M=M;
		keys= (Key[]) new Object[M];
		values = (Value[]) new Object[M];
		
	}
	
	private int hash(Key key)
	{
		return (key.hashCode() & 0x7fffffff) % M;
	}
	
	public void rehash(int s)
	{
		LinearProbingHashST<Key, Value> nuevo= new LinearProbingHashST<Key, Value>(s);
		for (int i = 0; i<M; i++)
		{
			if(keys[i]!=null)
			{
				nuevo.put(keys[i], values[i]);
			}
		}
		keys=nuevo.keys;
		values=nuevo.values;
		M=nuevo.M;
	}
	
	public void put(Key key, Value val)
	{
		if(N/M>0.75) rehash(M*2);
		int i;
		for (i= hash(key); keys[i]!=null; i=(i+1)%M)
		{
			if(keys[i].equals(key)){
				values[i]=(Value) val;
				return;}
		}
			keys[i]=key;
			values[i]=(Value) val;
			N++;

	}
	
	
	public Value get(Key key)
	{
		if(!contains(key)) 
			{
			return null;
			}
		for (int i=hash(key); keys[i]!=null; i=(i+1)%M)
		{
			if(keys[i].equals(key)) 	
				{
				return (Value) values[i];			
				}
		}
		return null;
	}
	
	public void delete(Key key)
	{
		if(!contains(key)) return;
		int i =hash(key);
		while(keys[i]!=null)
		{
			if(keys[i].equals(key)) break;
			i=(i+1)%M;
		}
		keys[i]=null;
		values[i]=null;
		i=(i+1)%M;
		while(keys[i]!=null)
		{
			Key kTemp=keys[i];
			Value vTemp=values[i];
			keys[i]=null;
			values[i]=null;
			N--;
			put(kTemp, vTemp);
			i=(i+1)%M;
		}
		N--;
	}
	
	public DoubleLinkedList<Key> keys()
	{
		DoubleLinkedList<Key> st=new DoubleLinkedList<Key>();
		
		for (int i=0; i<keys.length; i++)
		{
			if(keys[i]!=null) st.add(keys[i]);	
		}
		return st;
	}
	
	public boolean contains(Key key)
	{
		int i=hash(key);
		while(keys[i]!=null)
		{	
			if(keys[i].equals(key)) return true;
				
			i=(i+1)%M;	
		}
		return false;
	}
	
	public int size()
	{
		return M;
	}
}
