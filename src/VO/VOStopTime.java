package VO;

public class VOStopTime {
	
	private int TripId;
	private String ArrivalTime;
	private String DepartureTime;
	private int StopId;
	private int stopSequence;
	private Double distTraveled;


	public VOStopTime (int tId, String arrTime, String depTime, int sId,int sequence,double distTra)
	{
	    StopId=sId;
		ArrivalTime=arrTime;
		DepartureTime= depTime;
		stopSequence= sequence;
		TripId=tId;
		distTraveled=distTra;
	}

	public int Id()
	{ return StopId;}

	public String ArrivalTime()
	{return ArrivalTime;
	}

	public String DepartureTime(){
		return DepartureTime;
	}
	public int TripId()
	{return TripId;}


	public int Sequence(){
		return stopSequence;}
	
	public double distTraveled()
	{return distTraveled;	}


}
