package VO;

import DataStructures.DoubleLinkedList;
import DataStructures.LinearProbingHashST;

public class VOStop {
	
	private int Id;
	private int Code;
	private String Name;
	private double Lat;
	private double Lon;
	private String ZoneId;
	private DoubleLinkedList<VOStopTime> trips;
	
	public VOStop(int id,int code, String name,double lat, double lon,String zoneid, DoubleLinkedList<VOStopTime> viajes){
	
		Id=id;
		Code=code;
		Name=name;
		Lat=lat;
		Lon=lon;
		ZoneId=zoneid;
		trips=viajes;
				}
	
	public int Id() {
			return Id;
	}
	public int Code() {
		return Code;
}
	public String getName() {
	
		return Name;
	}
	public double Lat() {
		return Lat;
}
	public double Lon() {
		return Lon;
}
	public String ZoneId() {
		
		return ZoneId;
	}

	public DoubleLinkedList<VOStopTime> stopTimes()
	{
		return trips;
	}
}
